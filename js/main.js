const btnListar = document.querySelector('.btnListar');
const btnLimpiar = document.querySelector('.btnLimpiar');
const alcoholicRadio = document.querySelector('#alcoholic');
const nonAlcoholicRadio = document.querySelector('#non-alcoholic');
const main_container = document.querySelector('.container-main');
const total = document.querySelector('.total');

const llamandoFetch = async (url) => {
    // wheres God
    try {
        const respuesta = await fetch(url);
        const resultado = await respuesta.json();
        if (Object.keys(resultado).length === 0) {
            throw new Error("No hay resultados");
        }
        return resultado;
    }
    catch(error) {
        alert(error.message);
    }
}

btnListar.addEventListener('click', async () => {
    const drink_type =
        alcoholicRadio.checked ? 'Alcoholic' :
        nonAlcoholicRadio.checked ? 'Non_Alcoholic' :
        'Otro';

    if (drink_type === `Otro`) { // Primera  validación
        alert("Elige una opción para listar.");
        return;
    }
    const drinks = await llamandoFetch(`https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=${drink_type}`);

    main_container.innerHTML = ``; // Se limpia

    for (const drink of drinks["drinks"]) { // Se inserta la información
        main_container.innerHTML +=
        `
            <div class="card">
                <img class="drink_img" src="${drink.strDrinkThumb}" alt="${drink.strDrink}" />
                <p>${drink.strDrink}</p>
            </div>
        `
    }
    total.innerHTML = `${drinks["drinks"].length}`;
});

btnLimpiar.addEventListener('click', () => {
    main_container.innerHTML = ``;
    total.innerHTML = `0`;
});
